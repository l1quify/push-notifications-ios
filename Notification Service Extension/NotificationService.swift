//
//  NotificationService.swift
//  Notification Service Extension
//
//  Created by Gleb Arkhipov on 30/10/2018.
//  Copyright © 2018 Gleb Arkhipov. All rights reserved.
//

import UserNotifications

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(
        _ request: UNNotificationRequest,
        withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void
    ) {
        
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if
            let bestAttemptContent = bestAttemptContent,
            let ciphertext = bestAttemptContent.userInfo["encrypted"] as? String
        {

            bestAttemptContent.title = "\(bestAttemptContent.title) [Secret Message]"
            bestAttemptContent.userInfo["secret_key"] = decrypt(ciphertext: ciphertext)

            contentHandler(bestAttemptContent)
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let bestAttemptContent =  bestAttemptContent {
            
            contentHandler?(bestAttemptContent)
        }
    }
    
    func decrypt(ciphertext: String) -> String {
        
        let ciphertextBytes = [UInt8](ciphertext.utf8)
        var plaintextBytes = [UInt8]()
        
        ciphertextBytes.forEach { element in
            
            plaintextBytes.append(element - 3)
        }
        
        return String(data: Data(bytes: plaintextBytes), encoding: .utf8)!
    }
}
