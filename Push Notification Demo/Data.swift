//
//  Data.swift
//  Push Notification Demo
//
//  Created by Gleb Arkhipov on 30/10/2018.
//  Copyright © 2018 Gleb Arkhipov. All rights reserved.
//

import Foundation

extension Data {
    
    public var stringValue: String {
        
        return reduce("", { $0 + String(format: "%02X", $1) })
    }
}
