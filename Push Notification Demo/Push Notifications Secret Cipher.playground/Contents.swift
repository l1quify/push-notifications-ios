//: Playground - noun: a place where people can play

import Foundation

func encrypt(plaintext: String) -> String {
    
    let plaintextBytes = [UInt8](plaintext.utf8)
    var ciphertextBytes = [UInt8]()
    
    plaintextBytes.forEach { element in
        
        ciphertextBytes.append(element + 3)
    }
    
    return String(data: Data(bytes: ciphertextBytes), encoding: .utf8)!
}

func decrypt(ciphertext: String) -> String {
    
    let ciphertextBytes = [UInt8](ciphertext.utf8)
    var plaintextBytes = [UInt8]()
    
    ciphertextBytes.forEach { element in
        
        plaintextBytes.append(element - 3)
    }
    
    return String(data: Data(bytes: plaintextBytes), encoding: .utf8)!
}

let encrypted = encrypt(plaintext: "Super long sentence to encrypt")
print(encrypted)
let decrypted = decrypt(ciphertext: encrypted)
print(decrypted)
