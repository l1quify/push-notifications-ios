//
//  ViewController.swift
//  Push Notification Demo
//
//  Created by Gleb Arkhipov on 30/10/2018.
//  Copyright © 2018 Gleb Arkhipov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var deviceTokenLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var decryptedMessages = [String]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        
        deviceTokenLabel.text = "Waiting for registration..."
        
        registerForNotifications()
        
        reloadData()
    }
    
    func registerForNotifications() {
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(receivedRemoteNotification),
            name: .receivedRemoteNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(registeredForRemoteNotifications),
            name: .registeredForRemoteNotifications,
            object: nil
        )
    }
    
    func reloadData() {
        
        let messages =  UserDefaults.standard.array(forKey: "decrypted.messages") as? [String]
        
        decryptedMessages = messages ?? []
        tableView.reloadData()
    }
    
    @IBAction func touchUpInsideDeleteButton(_ sender: UIButton) {
        
        UserDefaults.standard.removeObject(forKey: "decrypted.messages")
    }
    
    @IBAction func touchUpInsideReloadButton(_ sender: UIButton) {
        
        reloadData()
    }
    
    @objc func receivedRemoteNotification() {
        
        reloadData()
    }
    
    @objc func registeredForRemoteNotifications() {
        
        guard let deviceToken = (UIApplication.shared.delegate as? AppDelegate)?.deviceToken else {
            
            return
        }
        
        deviceTokenLabel.text = deviceToken.stringValue
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return decryptedMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        cell.textLabel?.text = decryptedMessages[indexPath.row]
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.lineBreakMode = .byWordWrapping
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
