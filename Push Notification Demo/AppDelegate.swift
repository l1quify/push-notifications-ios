//
//  AppDelegate.swift
//  Push Notification Demo
//
//  Created by Gleb Arkhipov on 30/10/2018.
//  Copyright © 2018 Gleb Arkhipov. All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var deviceToken: Data?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        registerForPushNotifications()

        return true
    }

    func registerForPushNotifications() {
        
        UNUserNotificationCenter.current().delegate = self
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .alert, .sound]) { (granted, error) in
            
            DispatchQueue.main.async {
                
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        willPresent notification: UNNotification,
        withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void
    ) {
        
        UINotificationFeedbackGenerator().notificationOccurred(.success)
        
        completionHandler([.alert])
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print(error)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        self.deviceToken = deviceToken
        
        print("Device token: \(deviceToken.stringValue)")
        
        NotificationCenter.default.post(name: .registeredForRemoteNotifications, object: nil)
    }
    
    func application(
        _ application: UIApplication,
        didReceiveRemoteNotification userInfo: [AnyHashable : Any],
        fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void
    ) {

        var oldMessages = UserDefaults.standard.array(forKey: "decrypted.messages") as? [String] ?? []

        guard let newMessage = userInfo["secret_key"] as? String else {

            completionHandler(.failed)

            return
        }

        oldMessages.append(newMessage)

        UserDefaults.standard.set(oldMessages, forKey: "decrypted.messages")

        NotificationCenter.default.post(name: .receivedRemoteNotification, object: nil)

        completionHandler(.newData)
    }
}

extension Notification.Name {
    
    public static let registeredForRemoteNotifications = Notification.Name(rawValue: "registeredForRemoteNotifications")
    public static let receivedRemoteNotification = Notification.Name(rawValue: "receivedRemoteNotification")
}
